<?php
 include("db.php");
 if(isset($_POST['username'])){
     $nama = $_POST['nama'];
     $username = $_POST['username'];
     $password = $_POST['password'];

     $sql = "INSERT INTO tbl_admin (nama, username, password) VALUE('$nama', '$username', '$password')";
     $query = mysqli_query($db, $sql);

     if($query){
        echo "<script>alert('Berhasil Menginput Data')</script>";
     }else{
        echo "<script>alert('Kesalahan Saat Menginput Data')</script>";
     }
 }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>Admin - Komfikom</title>
    <!-- General CSS Files -->
    <link rel="stylesheet" href="assets/css/app.min.css">
    <!-- Template CSS -->
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/components.css">
    <!-- Custom style CSS -->
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel='shortcut icon' type='image/x-icon' href='/admin/assets/img/favicon.ico'/>
    <script src="assets/bundles/izitoast/js/iziToast.min.js"></script>
<link rel="stylesheet" href="assets/bundles/izitoast/css/iziToast.min.css">
<link rel="stylesheet" href="assets/bundles/datatables/datatables.min.css">
<link rel="stylesheet" href="assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">

</head>

<body class="dark dark-sidebar theme-black">
<div class="loader"></div>
<div id="app">
    <div class="main-wrapper main-wrapper-1">
        <div class="navbar-bg"></div>
        <nav class="navbar navbar-expand-lg main-navbar">
            <div class="form-inline mr-auto">
                <ul class="navbar-nav mr-3">
                    <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg
									collapse-btn"> <i data-feather="align-justify"></i></a></li>
                    <li><a href="#" class="nav-link nav-link-lg fullscreen-btn">
                            <i data-feather="maximize"></i>
                        </a></li>
                    <li>
                        <form class="form-inline mr-auto">
                            <div class="search-element">
                                <input class="form-control" type="search" placeholder="Search" aria-label="Search"
                                       data-width="200">
                                <button class="btn" type="submit">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </form>
                    </li>
                </ul>
            </div>


        </nav>
        <div class="main-sidebar sidebar-style-2">
            <aside id="sidebar-wrapper">
                <div class="sidebar-brand">
                    <h3>Admin Docmed</h3>
                </div>
                <div class="sidebar-user">
                    <div class="sidebar-user-picture">
                        <img alt="image" src="assets/img/user.png">
                    </div>
                    <div class="sidebar-user-details">
                        <div class="user-name">Admin</div>
                    </div>
                </div>
                <ul class="sidebar-menu">
                
                    <li class="menu-header">Admin</li>
                    <li><a class="nav-link" href="/kesehatan/admin/table-admin.php"><i data-feather="user"></i><span>Database Admin</span></a>
                    </li>
                    <li><a class="nav-link" href="/kesehatan/admin/form-visi.php"><i data-feather="edit"></i><span>Form Visi Misi</span></a>
                    </li>
                    <li><a class="nav-link" href="/kesehatan/admin/form-kontak.php"><i data-feather="edit"></i><span>Form Kontak</span></a>
                    </li>
                    <li><a class="nav-link" href="/kesehatan/admin/form-about.php"><i data-feather="edit"></i><span>Form About</span></a>
                    </li>
                    </li><li><a class="nav-link" href="/kesehatan/admin"><i data-feather="log-out"></i><span>Logout</span></a>
                    </li>


                </ul>
            </aside>
        </div>
        <!-- Main Content -->
        <div class="main-content">
            <section class="section">
                <div class="section-body">
                    <!-- add content here -->
                    <div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">

                <div class=" col-6">
                    <h4>Table Admin</h4>
                </div>
                <div class="d-flex justify-content-end col-6">
                    <button href="#"  data-toggle="modal"
                            data-target="#exampleModal" class="btn btn-lg btn-icon icon-left btn-primary">
                        <i class="far fa-edit"></i> Tambah</button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="table-1" class="table table-admin table-striped" >
                        <thead>
                        <tr>

                            <th>Nama Lengkap</th>
                        
                            <th>Username</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                                $sql = "SELECT * FROM tbl_admin";
                                $query = mysqli_query($db, $sql);
                                
                                while($row = mysqli_fetch_array($query)){
                            ?>
                            <tr>
                                <td><?php echo $row['nama'] ?></td>
                                <td><?php echo $row['username'] ?></td>
                                <td>-</td>
                            </tr>
                                <?php }?>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
                   
                </div>
            </section>
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="formModal">Form Admin</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form  method="POST">
                    <div class="form-group">
                        <label>Nama Lengkap</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fas fa-address-card"></i>
                                </div>
                            </div>
                            <input name="nama" type="text" class="form-control" placeholder="Nama Lengkap">
                        </div>
                    </div>
    
                    <div class="form-group">
                        <label>Username</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fas fa-user"></i>
                                </div>
                            </div>
                            <input name="username" type="text" class="form-control" placeholder="Username">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fas fa-lock"></i>
                                </div>
                            </div>
                            <input name="password" type="password" class="form-control" placeholder="password">
                        </div>
                    </div>


                    <button id="submit" class="btn btn-primary m-t-15 waves-effect">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
           

        </div>
        <footer class="main-footer">
            <div class="footer-left">
                Copyright &copy; 2019
                <div class="bullet"></div>
                DOUBLEHELIX
            </div>
            <div class="footer-right">
            </div>
        </footer>
    </div>
</div>
<!-- General JS Scripts -->
<script src="assets/bundles/datatables/datatables.min.js"></script>
<script src="assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="assets/js/page/datatables.js"></script>
<script src="assets/bundles/sweetalert/sweetalert.min.js"></script>
<script src="assets/js/app.min.js"></script>
<!-- JS Libraies -->
<!-- Page Specific JS File -->
<!-- Template JS File -->
<script src="assets/js/scripts.js"></script>
<!-- Custom JS File -->
<script src="assets/js/custom.js"></script>

</body>


</html>